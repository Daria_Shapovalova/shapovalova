package com.example.developerslife.ui.main

import androidx.lifecycle.*
import com.example.developerslife.app.MyApp
import com.example.developerslife.database.DatabaseGif
import com.example.developerslife.database.asDomainModel
import com.example.developerslife.database.getDatabase
import com.example.developerslife.network.DevelopersLifeAPI
import com.example.developerslife.network.asDatabaseModel
import kotlinx.coroutines.*

enum class DevelopersLifeApiStatus { LOADING, ERROR, DONE }

class PageViewModel() : ViewModel() {

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val application = MyApp.context!!
    private val database = getDatabase(application)

    private val _status = MutableLiveData<DevelopersLifeApiStatus>()

    val status: LiveData<DevelopersLifeApiStatus>
        get() = _status

    private val _gif = MutableLiveData<Gif>()

    val gif: LiveData<Gif>
        get() = _gif

    private val _backButtonActiveState = MutableLiveData<Boolean>()

    val backButtonActiveState: LiveData<Boolean>
        get() = _backButtonActiveState

    init {
        coroutineScope.launch {
            val databaseEmpty = checkIfDatabaseIsEmpty()
            if (databaseEmpty) {
                getRandomGIF()
                _backButtonActiveState.value = false
            } else {
                val currentGif = getCurrentGifFromDatabase()
                _gif.value = currentGif.asDomainModel()
                _backButtonActiveState.value = currentGif.id != 1L
            }
        }
    }

    private fun getRandomGIF() {
        coroutineScope.launch {
            val getGIFDeferred = DevelopersLifeAPI.retrofitService.getGIF()
            try {
                _status.value = DevelopersLifeApiStatus.LOADING
                val result = getGIFDeferred.await()
                _status.value = DevelopersLifeApiStatus.DONE
                val newGif = result.asDatabaseModel()
                newGif.currentGif = 1
                addNewGifToDatabase(newGif)
                _gif.value = newGif.asDomainModel()
            } catch (t: Throwable) {
                _status.value = DevelopersLifeApiStatus.ERROR
            }
        }
    }

    fun onNext() {
        coroutineScope.launch {
            val databaseHasNext = checkIfDatabaseHasNext()
            if (databaseHasNext) {
                val currentGif = getCurrentGifFromDatabase()
                currentGif.currentGif = 0
                updateGifInTheDatabase(currentGif)
                val nextGif = getGifFromDatabaseById(currentGif.id + 1)
                nextGif.currentGif = 1
                updateGifInTheDatabase(nextGif)
                _gif.value = nextGif.asDomainModel()
                _status.value = DevelopersLifeApiStatus.DONE
                _backButtonActiveState.value = true
            } else {
                val getGIFDeferred = DevelopersLifeAPI.retrofitService.getGIF()
                try {
                    _status.value = DevelopersLifeApiStatus.LOADING
                    val result = getGIFDeferred.await()
                    _status.value = DevelopersLifeApiStatus.DONE
                    val currentGif = getCurrentGifFromDatabase()
                    currentGif.currentGif = 0
                    updateGifInTheDatabase(currentGif)
                    val newGif = result.asDatabaseModel()
                    newGif.currentGif = 1
                    addNewGifToDatabase(newGif)
                    _gif.value = newGif.asDomainModel()
                    _backButtonActiveState.value = true
                } catch (t: Throwable) {
                    _status.value = DevelopersLifeApiStatus.ERROR
                }
            }
        }
    }

    fun onBack() {
        coroutineScope.launch {
            val currentGif = getCurrentGifFromDatabase()
            if (currentGif.id > 1) {
                currentGif.currentGif = 0
                updateGifInTheDatabase(currentGif)
                val previousGif = getGifFromDatabaseById(currentGif.id - 1)
                previousGif.currentGif = 1
                updateGifInTheDatabase(previousGif)
                _gif.value = previousGif.asDomainModel()
                _status.value = DevelopersLifeApiStatus.DONE
                if (previousGif.id == 1L) {
                    _backButtonActiveState.value = false
                }
            }
        }
    }

    private suspend fun checkIfDatabaseIsEmpty(): Boolean {
        return withContext(Dispatchers.IO) {
            database.gifDao.getCurrentGifId() == 0L
        }
    }

    private suspend fun checkIfDatabaseHasNext(): Boolean {
        return withContext(Dispatchers.IO) {
            val currentId = database.gifDao.getCurrentGifId()
            val maxId = database.gifDao.getMaxId()
            currentId != maxId
        }
    }

    private suspend fun updateGifInTheDatabase(gif: DatabaseGif) {
        return withContext(Dispatchers.IO) {
            database.gifDao.updateGif(gif)
        }
    }

    private suspend fun getCurrentGifFromDatabase(): DatabaseGif {
        return withContext(Dispatchers.IO) {
            database.gifDao.getCurrentGif()
        }
    }

    private suspend fun addNewGifToDatabase(gif: DatabaseGif) {
        withContext(Dispatchers.IO) {
            database.gifDao.insert(gif)
        }
    }

    private suspend fun getGifFromDatabaseById(id: Long): DatabaseGif {
        return withContext(Dispatchers.IO) {
            database.gifDao.getGifById(id)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private val _index = MutableLiveData<Int>()

    val text: LiveData<String> = Transformations.map(gif) {
        gif.value?.description
    }

    val gifURL: LiveData<String> = Transformations.map(gif) {
        gif.value?.gifURL
    }

    fun setIndex(index: Int) {
        _index.value = index
    }

}