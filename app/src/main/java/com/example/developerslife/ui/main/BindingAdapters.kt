package com.example.developerslife.ui.main

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.developerslife.R

@BindingAdapter("gifURL")
fun bindImage(imgView: ImageView, gifURL: String?) {
    gifURL?.let {
        val imgUri = gifURL.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .into(imgView)
    }
}

@BindingAdapter("developersLifeApiStatus")
fun bindStatus(statusImageView: ImageView, status: DevelopersLifeApiStatus?) {
    when (status) {
        DevelopersLifeApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_baseline_hourglass_empty_24)
        }
        DevelopersLifeApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_baseline_cloud_off_24)
        }
        DevelopersLifeApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}

@BindingAdapter("developersLifeApiStatusGif")
fun bindStatusGif(imageView: ImageView, status: DevelopersLifeApiStatus?) {
    when (status) {
        DevelopersLifeApiStatus.LOADING -> {
            imageView.visibility = View.GONE
        }
        DevelopersLifeApiStatus.ERROR -> {
            imageView.visibility = View.GONE
        }
        DevelopersLifeApiStatus.DONE -> {
            imageView.visibility = View.VISIBLE
        }
    }
}

@BindingAdapter("developersLifeApiStatusText")
fun bindStatusText(textView: TextView, status: DevelopersLifeApiStatus?) {
    when (status) {
        DevelopersLifeApiStatus.LOADING -> {
            textView.visibility = View.GONE
        }
        DevelopersLifeApiStatus.ERROR -> {
            textView.visibility = View.GONE
        }
        DevelopersLifeApiStatus.DONE -> {
            textView.visibility = View.VISIBLE
        }
    }
}

@BindingAdapter("buttonActiveState")
fun bindButtonActiveState(button: Button, activeState: Boolean) {
    when (activeState) {
        true -> {
            button.isEnabled = true
        }
        false -> {
            button.isEnabled = false
        }
    }
}