package com.example.developerslife.ui.main

import com.example.developerslife.database.DatabaseGif

class Gif constructor(
    val id: Long,
    val description: String,
    val gifURL: String,
)

fun Gif.asDatabaseModel(): DatabaseGif {
    return DatabaseGif(
        id = this.id,
        description = this.description,
        gifURL = this.gifURL
    )
}

