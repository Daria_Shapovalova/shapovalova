package com.example.developerslife.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.developerslife.ui.main.Gif

@Entity
class DatabaseGif constructor(

    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    val description: String,
    val gifURL: String,
    var currentGif: Int = 0

    )

fun DatabaseGif.asDomainModel(): Gif {
    return Gif(
        id = this.id,
        description = this.description,
        gifURL = this.gifURL
    )
}