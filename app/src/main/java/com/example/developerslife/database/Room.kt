package com.example.developerslife.database

import android.content.Context
import androidx.room.*

@Dao
interface GifDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(gif: DatabaseGif)

    @Query("SELECT id FROM databasegif WHERE currentGif = 1 LIMIT 1")
    fun getCurrentGifId(): Long

    @Query("SELECT * FROM databasegif WHERE currentGif = 1")
    fun getCurrentGif(): DatabaseGif

    @Update
    fun updateGif(databaseGif: DatabaseGif)

    @Query("SELECT id FROM databasegif ORDER BY id DESC LIMIT 1")
    fun getMaxId(): Long

    @Query("SELECT * FROM databasegif ORDER BY id DESC LIMIT 1")
    fun getLastGif(): DatabaseGif

    @Query("SELECT * FROM databasegif ORDER BY id ASC LIMIT 1")
    fun getFirstGif(): DatabaseGif

    @Query("SELECT * FROM databasegif WHERE id = :id")
    fun getGifById(id: Long): DatabaseGif

}

@Database(entities = [DatabaseGif::class], version = 15)
abstract class GifsDatabase : RoomDatabase() {
    abstract val gifDao: GifDao

}

private lateinit var INSTANCE: GifsDatabase

fun getDatabase(context: Context): GifsDatabase {
    synchronized(GifsDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                GifsDatabase::class.java,
                "gifs"
            ).fallbackToDestructiveMigration().build()
        }
    }
    return INSTANCE
}
